process.env.ENV = 'test'

//Подключаем dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

chai.use(chaiHttp);


describe('GET /api/utils/search-github-repo', () => {
  it('lodash', (done) => {
    chai.request(server)
      .get('/api/utils/search-github-repo?search=lodash')
      .end((err, res) => {
        res.should.have.status(200)
        res.body.items.should.be.a('array')
        done()
      })
  })

  it('lodash, page 7', (done) => {
    chai.request(server)
      .get('/api/utils/search-github-repo?search=lodash&page=7')
      .end((err, res) => {
        res.should.have.status(200)
        res.body.items.should.be.a('array')
        done()
      })
  })
})
