process.env.ENV = 'test'

const searchInGithub = require('./../server/utils/github.js')
const assert = require('assert')

describe("github search", function() {
  it(`lodash`, async () => {
    const results = await searchInGithub({
      search: 'lodash'
    })
    assert(typeof results.total_count === 'number', 'total_count must be a number')
    assert(typeof results.incomplete_results === 'boolean', 'incomplete_results must be a boolean')
    assert(Array.isArray(results.items), 'items must be an array')
  })

  it(`jquery`, async () => {
    const results = await searchInGithub({
      search: 'jquery'
    })
    assert(typeof results.total_count === 'number', 'total_count must be a number')
    assert(typeof results.incomplete_results === 'boolean', 'incomplete_results must be a boolean')
    assert(Array.isArray(results.items), 'items must be an array')
  })

})
