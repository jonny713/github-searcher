const bent = require('bent')

const get = bent('https://api.github.com/search', 'GET', 'json', 200, {
  accept: "application/vnd.github.v3+json",
  'User-Agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1521.3 Safari/537.36'
})

const template = {
  total_count: 0,
  incomplete_results: true,
  items: []
}


async function searchInGithub({ search, page = 1, per_page = 30, order = 'desc', sort = 'best match' }) {
  let result
  try {
    result = await get(`/repositories?q=${
      encodeURI(search)
    }&page=${
      page
    }&per_page=${
      per_page
    }&order=${
      order
    }&sort=${
      sort
    }`)
  } catch (e) {
    console.error(e)
  }
  // console.log(result)

  if (result) {
    return {
      total_count: result.total_count || template.total_count,
      incomplete_results: result.incomplete_results || template.incomplete_results,
      items: result.items || template.items
    }
  }

  return template
}


module.exports = searchInGithub
