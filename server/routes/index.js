const assert = require('assert')

const express = require('express')
const router = express.Router()

const searchInGithub = require('./../utils/github.js')


/**
 *
 * @typedef {object} UtilsSearchGithubRepoRequest
 * @property {string} search.required - Строка поиска
 * @property {number} page - Страница - default:1
 * @property {number} per_page - Кол-во элементов на странице - default:30
 * @property {string} order - Порядок - default:desc - enum:asc,desc
 * @property {string} sort - Сортировка - default:best match - enum:undefined,stars,forks,help-wanted-issues,updated
 */

/**
 * Результирующий объект с ответом
 * @typedef {object} UtilsSearchGithubRepoResponse
 * @property {number} total_count - Общая кол-во элементов
 * @property {number} incomplete_results -
 * @property {array<object>} items - элементы
 */

/**
 * GET /api/utils/search-github-repo
 * @api
 * @summary Поиск репозиториев по строке
 * @param {UtilsSearchGithubRepoRequest} request.query.required
 * @return {UtilsSearchGithubRepoResponse} 200 - success response - application/json
 */
router.get('/utils/search-github-repo', async (req, res) => {
  const { search, page, per_page, order, sort } = req.query

  try {
    // Проверяем корректность полей
    assert(search !== undefined, 'Требуется строка поиска')
    assert(page === undefined || !isNaN(page), 'Страница должна быть числом')
    assert(per_page === undefined || !isNaN(per_page), 'Страница должна быть числом')
    assert([undefined,'asc','desc'].includes(order), 'Порядок(order) должен иметь значение asc или desc')
    assert([undefined,'stars','forks','help-wanted-issues','updated'].includes(order), 'Сортировка(sort) должен иметь значение stars, forks, help-wanted-issues или updated')


  } catch (e) {
    console.error(e)

    return res.status(400).send({ error: e.message })
  }

  try {
    let result = await searchInGithub({ search, page, per_page, order, sort })
    res.send(result)
  } catch (e) {
    console.error(e)

    res.status(500).send({ error: e.message })
  }
})




module.exports = router;
