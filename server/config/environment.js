/**
* Подключаем параметры типа окружения
*/
const {
  ENV
} = process.env

module.exports = {
  type: ENV || 'production'
}
