const path = require('path')
require('dotenv').config({ path: path.join(__dirname, '../.env') })

const http = require('./http')
const environment = require('./environment')

module.exports = {
    http,
    environment
}
