/**
* Подключаем параметры для http сервера
*/
const {
  PORT,
  TEST_PORT
} = process.env

module.exports = {
  port: PORT || 3000,
  testPort: TEST_PORT || 3001
}
