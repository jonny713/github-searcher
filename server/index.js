const { http, environment } = require('./config')
const express = require('express')
const app = express()
const path = require('path');

if (environment.type === 'develop') {
  const logger = require('@vl-utils/log')()

  const expressJSDocSwagger = require('express-jsdoc-swagger')
  const options = {
    info: {
      version: '1.0.0',
      title: 'Github-searcher API'
    },
    filesPattern: './routes/**/*.js',
    baseDir: __dirname,
  }

  // Подключаем документацию API
  expressJSDocSwagger(app)(options)
}


// Включаем парсер параметров запроса
app.use(express.json({ limit: '50mb' }))
app.use(express.urlencoded({ limit: '50mb', extended: false }))

// Указываем шаблонизатор
app.set('view engine', 'pug');

// Запускаем прослушивание
let server
if (environment.type === 'test') {
  server = app.listen(http.testPort)
} else {
  server = app.listen(http.port)
  console.log(`Сервер запущен, порт ${http.port}`);
}

// Подключаем маршруты
const api = require('./routes')
app.use('/api', api)


app.get('/client.js', (req, res) => {
  res.sendFile(path.join(__dirname, '..', 'client', 'dist', 'client.js'));
})

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '..', 'client', 'dist', 'index.html'));
})

module.exports = app
