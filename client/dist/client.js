let formEl = document.querySelector('.form')
let textEl = document.querySelector('.form').searchInput
let submitEl = document.querySelector('.form').submitInput

let resultsEl = document.querySelector('.results')

const state = {
  search: '',
  page: 1
}

formEl.addEventListener('submit', async (event) => {
  event.preventDefault()

  state.search = textEl.value
  state.page = 1

  updateData()
})

async function updateData() {
  let result = await searchGithubRepos(state)

  updateResult(result.items)
}

function searchGithubRepos({search, page}) {
  return fetch(`/api/utils/search-github-repo?search=${search}&page=${page}`, {
    method: "get",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }).then(response => response.json())
}

function clearResults() {
  try {
    resultsEl.prevPage.removeEventListener('click')
    resultsEl.nextPage.removeEventListener('click')
  } catch (e) {}

  resultsEl.innerHTML = ''
}

function updateResult(items) {
  clearResults()

  for (let item of items) {
    addRow(item)
  }

  addButton('prevPage', event => {
    state.page -= 1

    updateData()
  })
  addButton('nextPage', event => {
    state.page += 1

    updateData()
  })
}

function addRow(item) {
  const el = document.createElement("li")

  const a = document.createElement("a")
  a.setAttribute('href', item.url)
  a.textContent = item.name

  const i = document.createElement("i")
  i.textContent = item.description
  
  el.append(a)
  el.append(i)

  resultsEl.append(el)
}

function addButton(name, eventAction) {
  const el = document.createElement("button")
  el.textContent = name
  el.name = name
  el.addEventListener('click', eventAction)

  resultsEl.append(el)

}
